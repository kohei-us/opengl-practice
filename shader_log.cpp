
#include <GL/glew.h>
#include <GL/glut.h>

#include <iostream>
#include <cstdlib>
#include <vector>

#define STRINGIFY(...) #__VA_ARGS__"\n"

using namespace std;

// Shader source contains an intentional syntax error.
const char* vs_src = STRINGIFY(
\n
\n#version 430 core
\n
layout (location = 0) out vec4 color;\n
\n
uniform vec4 scale;\n
uniform vec3 bias;\n
\n
void main()\n
{\n
    color = vec4(1.0, 0.5, 0.2, 1.0) * scale + bias;\n
}\n
\n
);

void repaint() {}

void reshape(int, int) {}

void idle() {}

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowSize(1024, 768);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Many cubes!");

    glutDisplayFunc(repaint);
    glutReshapeFunc(reshape);
    glutIdleFunc(idle);

    glewInit();

    GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex_shader, 1, &vs_src, NULL);
    glCompileShader(vertex_shader);

    GLint log_length;
    glGetShaderiv(vertex_shader, GL_INFO_LOG_LENGTH, &log_length);
    cout << "log length: " << log_length << endl;
    GLint status;
    glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &status);
    cout << "compile status: " << status << endl;

    vector<GLchar> str(log_length);
    glGetShaderInfoLog(vertex_shader, log_length, NULL, &str[0]);
    cout << &str[0] << endl;

    glDeleteShader(vertex_shader);

//  glutMainLoop();

    return EXIT_SUCCESS;
}
