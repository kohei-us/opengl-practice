
#include <cstdlib>
#include <GL/glew.h>
#include <GL/glut.h>

#include <iostream>
#include <cmath>

#define STRINGIFY(...) #__VA_ARGS__"\n"

using namespace std;

const char* vs_src = STRINGIFY(
\n
\n#version 430 core
\n
layout (location = 0) in vec4 offset;
layout (location = 1) in vec4 color;

out vec4 vs_color;

void main()
{
    const vec4 vertices[3] = vec4[3](
        vec4( 0.25, -0.25, 0.5, 1.0),
        vec4(-0.25, -0.25, 0.5, 1.0),
        vec4( 0.25,  0.25, 0.5, 1.0)
    );
    gl_Position = vertices[gl_VertexID] + offset;
    vs_color = color;
}

);

const char* fs_src = STRINGIFY(
\n
\n#version 430 core
\n
in vec4 vs_color;

out vec4 color;

void main()
{
    color = vs_color;
}

);

// tessellation control shader source
const char* tcs_src = STRINGIFY(
\n
\n#version 430 core
\n
layout (vertices = 3) out;

void main()
{
    if (gl_InvocationID == 0)
    {
        gl_TessLevelInner[0] = 5.0;
        gl_TessLevelOuter[0] = 5.0;
        gl_TessLevelOuter[1] = 5.0;
        gl_TessLevelOuter[2] = 5.0;
    }
    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
}

);

// tessellation evaluation shader source
const char* tes_src = STRINGIFY(
\n
\n#version 430 core
\n
layout (triangles, equal_spacing, cw) in;

void main()
{
    gl_Position = (gl_TessCoord.x * gl_in[0].gl_Position) +
                  (gl_TessCoord.y * gl_in[1].gl_Position) +
                  (gl_TessCoord.z * gl_in[2].gl_Position);
}

);

namespace {

double current_time = 0.0;

void idle()
{
    current_time += 0.02;
    glutPostRedisplay();
}

void repaint()
{
    const GLfloat color[] = {
        sin(current_time) * 0.5f + 0.5f,
        cos(current_time) * 0.5f + 0.5f,
        0.0f, 1.0f
    };
    glClearBufferfv(GL_COLOR, 0, color);

    GLfloat attrib[] = {
        sin(current_time) * 0.5f,
        cos(current_time) * 0.6f,
        0.0f, 0.0f
    };

    GLfloat vs_color[] = {
        1.0f - color[0],
        1.0f - color[1],
        1.0f - color[2],
        1.0f
    };
    glVertexAttrib4fv(0, attrib);
    glVertexAttrib4fv(1, vs_color);

    glDrawArrays(GL_PATCHES, 0, 3);

    glutSwapBuffers();
}

void reshape(int width, int height)
{
    cout << "reshape: width=" << width << ", height=" << height << endl;
}

class app
{
public:
    app() :
        m_program(0),
        m_vertex_shader(0),
        m_fragment_shader(0),
        m_tess_control_shader(0),
        m_tess_eval_shader(0),
        m_vertex_array_object(0)
    {
    }

    ~app()
    {
        if (m_vertex_array_object)
            glDeleteVertexArrays(1, &m_vertex_array_object);

        if (m_program)
            glDeleteProgram(m_program);

        if (m_vertex_shader)
            glDeleteShader(m_vertex_shader);

        if (m_fragment_shader)
            glDeleteShader(m_fragment_shader);

        if (m_tess_control_shader)
            glDeleteShader(m_tess_control_shader);

        if (m_tess_eval_shader)
            glDeleteShader(m_tess_eval_shader);
    }

    void init(int argc, char** argv)
    {
        glutInit(&argc, argv);
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
        glutInitWindowSize(1024, 768);
        glutInitWindowPosition(100, 100);
        glutCreateWindow("OpenGL Practice");

        glutDisplayFunc(repaint);
        glutReshapeFunc(reshape);
        glutIdleFunc(idle);

        glewInit();
        if (glewIsSupported("GL_VERSION_3_3"))
        {
            cout << "OpenGL 3.3 is supported" << endl;
        }
    }

    void run()
    {
        m_vertex_shader = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(m_vertex_shader, 1, &vs_src, NULL);
        glCompileShader(m_vertex_shader);

        m_fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(m_fragment_shader, 1, &fs_src, NULL);
        glCompileShader(m_fragment_shader);

        m_tess_control_shader = glCreateShader(GL_TESS_CONTROL_SHADER);
        glShaderSource(m_tess_control_shader, 1, &tcs_src, NULL);
        glCompileShader(m_tess_control_shader);

        m_tess_eval_shader = glCreateShader(GL_TESS_EVALUATION_SHADER);
        glShaderSource(m_tess_eval_shader, 1, &tes_src, NULL);
        glCompileShader(m_tess_eval_shader);

        m_program = glCreateProgram();
        glAttachShader(m_program, m_vertex_shader);
        glAttachShader(m_program, m_fragment_shader);
        glAttachShader(m_program, m_tess_control_shader);
        glAttachShader(m_program, m_tess_eval_shader);
        glLinkProgram(m_program);
        glUseProgram(m_program);

        glGenVertexArrays(1, &m_vertex_array_object);
        glBindVertexArray(m_vertex_array_object);

        // Set the number of control points per patch to 3.
//      glPatchParameteri(GL_PATCH_VERTICES, 3);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

        glutMainLoop();
    }

private:
    GLuint m_program;
    GLuint m_vertex_shader;
    GLuint m_fragment_shader;
    GLuint m_tess_control_shader;
    GLuint m_tess_eval_shader;
    GLuint m_vertex_array_object;
};

}

int main(int argc, char** argv)
{
    app _app;
    _app.init(argc, argv);
    _app.run();

    return EXIT_SUCCESS;
}
