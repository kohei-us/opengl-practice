
#include <GL/glew.h>
#include <GL/glut.h>

#include <iostream>
#include <cstdlib>
#include <vector>

#define STRINGIFY(...) #__VA_ARGS__"\n"

using namespace std;

// myFunction intentially not implemented, to get linker error message.
const char* vs_src = STRINGIFY(
\n
\n#version 430 core
\n
layout (location = 0) out vec4 color;\n
\n
vec3 myFunction();
\n
void main()\n
{\n
    color = vec4(myFunction(), 1.0);
}\n
\n
);

void repaint() {}

void reshape(int, int) {}

void idle() {}

GLint printProgramStatus(GLuint program, GLenum pname, const char* msg)
{
    GLint status;
    glGetProgramiv(program, pname, &status);
    cout << msg << ": " << status << endl;
    return status;
}

int main(int argc, char** argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
    glutInitWindowSize(1024, 768);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Many cubes!");

    glutDisplayFunc(repaint);
    glutReshapeFunc(reshape);
    glutIdleFunc(idle);

    glewInit();

    GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex_shader, 1, &vs_src, NULL);
    glCompileShader(vertex_shader);

    GLint status;
    glGetShaderiv(vertex_shader, GL_INFO_LOG_LENGTH, &status);
    cout << "log length: " << status << endl;
    glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &status);
    cout << "compile status: " << status << endl;

    vector<GLchar> str(status);
    glGetShaderInfoLog(vertex_shader, status, NULL, &str[0]);
    cout << "-- shader info" << endl;
    cout << &str[0] << endl;
    cout << "--" << endl;

    GLuint program = glCreateProgram();
    glAttachShader(program, vertex_shader);
    glLinkProgram(program);

    printProgramStatus(program, GL_LINK_STATUS, "link status");
    printProgramStatus(program, GL_ATTACHED_SHADERS, "attached shaders");
    printProgramStatus(program, GL_ACTIVE_ATTRIBUTES, "active attributes");
    printProgramStatus(program, GL_ACTIVE_UNIFORMS, "active uniforms");
    printProgramStatus(program, GL_ACTIVE_UNIFORM_BLOCKS, "active uniform blocks");
    status = printProgramStatus(program, GL_INFO_LOG_LENGTH, "info log length");
    str.resize(status);
    glGetProgramInfoLog(program, status, NULL, &str[0]);
    cout << "-- program info" << endl;
    cout << &str[0] << endl;
    cout << "--" << endl;

    glDeleteShader(vertex_shader);

    glDeleteProgram(program);

//  glutMainLoop();

    return EXIT_SUCCESS;
}
