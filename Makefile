
CPPFLAGS=-I/usr/include -I./include -g
LDFLAGS=`pkg-config --libs glew` -lglut

all: single-triangle many-cubes shader-log linker-log test

single-triangle: single_triangle.cpp
	$(CXX) -o $@ $(CPPFLAGS) $< $(LDFLAGS)

camera-position: camera_position.cpp
	$(CXX) -o $@ $(CPPFLAGS) $< $(LDFLAGS)

many-cubes: many_cubes.cpp
	$(CXX) -o $@ $(CPPFLAGS) $< $(LDFLAGS)

shader-log: shader_log.cpp
	$(CXX) -o $@ $(CPPFLAGS) $< $(LDFLAGS)

linker-log: linker_log.cpp
	$(CXX) -o $@ $(CPPFLAGS) $< $(LDFLAGS)

test: test.cpp
	$(CXX) -o $@ $(CPPFLAGS) $< $(LDFLAGS)
