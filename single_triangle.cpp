
#include <cstdlib>
#include <GL/glew.h>
#include <GL/glut.h>

#include <iostream>
#include <cmath>
#include <vector>

#define STRINGIFY(...) #__VA_ARGS__"\n"

using namespace std;

const char* vs_src = STRINGIFY(
\n
\n#version 430 core
\n
layout (location = 0) in vec4 offset;
layout (location = 1) in vec4 color;

out vec4 vs_color;

void main()
{
    const vec4 vertices[3] = vec4[3](
        vec4( 0.25, -0.25, 0.5, 1.0),
        vec4(-0.25, -0.25, 0.5, 1.0),
        vec4( 0.25,  0.25, 0.5, 1.0)
    );

    const vec4 colors[] = vec4[3](vec4(1.0, 0.0, 0.0, 1.0),
                                  vec4(0.0, 1.0, 0.0, 1.0),
                                  vec4(0.0, 0.0, 1.0, 1.0));

    gl_Position = vertices[gl_VertexID] + offset;
    vs_color = colors[gl_VertexID];
}

);

const char* vs_src_texture = STRINGIFY(
\n
\n#version 430 core
\n
void main()
{
    const vec4 vertices[] = vec4[](vec4( 0.75, -0.75, 0.5, 1.0),
                                   vec4(-0.75, -0.75, 0.5, 1.0),
                                   vec4( 0.75,  0.75, 0.5, 1.0));

    gl_Position = vertices[gl_VertexID];
}

);

const char* fs_src_checked = STRINGIFY(
\n
\n#version 430 core
\n
out vec4 color;

void main()
{
    color = vec4(sin(gl_FragCoord.x * 0.25) * 0.5 + 0.5,
                 cos(gl_FragCoord.y * 0.25) * 0.5 + 0.5,
                 sin(gl_FragCoord.x * 0.15) * cos(gl_FragCoord.y * 0.15),
                 1.0);
}

);

const char* fs_src_gradient = STRINGIFY(
\n
\n#version 430 core
\n
in vec4 vs_color;
out vec4 color;

void main()
{
    color = vs_color;
}

);

const char* fs_src_texture = STRINGIFY(
\n
\n#version 430 core
\n
uniform sampler2D s;

out vec4 color;

void main()
{
    color = texture(s, gl_FragCoord.xy / textureSize(s, 0));
}

);

namespace {

double current_time = 0.0;

void idle()
{
    current_time += 0.02;
    glutPostRedisplay();
}

void repaint()
{
    const GLfloat color[] = {
        sin(current_time) * 0.5f + 0.5f,
        cos(current_time) * 0.5f + 0.5f,
        0.0f, 1.0f
    };

    glClearBufferfv(GL_COLOR, 0, color);
    glDrawArrays(GL_TRIANGLES, 0, 3);

    glutSwapBuffers();
}

void reshape(int width, int height)
{
    cout << "reshape: width=" << width << ", height=" << height << endl;
}

void generate_texture(std::vector<float>& data, size_t x_size, size_t y_size)
{
    for (size_t x = 0; x < x_size; ++x)
    {
        for (size_t y = 0; y < y_size; ++y)
        {
            size_t offset = (y * x_size + x) * 4;
            data[offset  ] = (float)((x & y) & 0xFF) / 255.0f;
            data[offset+1] = (float)((x | y) & 0xFF) / 255.0f;
            data[offset+2] = (float)((x ^ y) & 0xFF) / 255.0f;
            data[offset+3] = 1.0f;
        }
    }
}

class app
{
public:
    app() :
        m_program(0),
        m_vertex_shader(0),
        m_fragment_shader(0),
        m_vertex_array_object(0),
        m_texture(0)
    {
    }

    ~app()
    {
        if (m_vertex_array_object)
            glDeleteVertexArrays(1, &m_vertex_array_object);

        if (m_program)
            glDeleteProgram(m_program);

        if (m_vertex_shader)
            glDeleteShader(m_vertex_shader);

        if (m_fragment_shader)
            glDeleteShader(m_fragment_shader);

        if (m_texture)
            glDeleteTextures(1, &m_texture);
    }

    void init(int argc, char** argv)
    {
        glutInit(&argc, argv);
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
        glutInitWindowSize(1024, 768);
        glutInitWindowPosition(100, 100);
        glutCreateWindow("Single triangle with texture");

        glutDisplayFunc(repaint);
        glutReshapeFunc(reshape);
        glutIdleFunc(idle);

        glewInit();
        if (glewIsSupported("GL_VERSION_3_3"))
        {
            cout << "OpenGL 3.3 is supported" << endl;
        }
    }

    void run()
    {
        // Set up texture.
        glGenTextures(1, &m_texture);
        glBindTexture(GL_TEXTURE_2D, m_texture);
        glTexStorage2D(GL_TEXTURE_2D, 8, GL_RGBA32F, 256, 256);

        std::vector<float> data(256*256*4, 0.0f);
        generate_texture(data, 256, 256);
        glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 256, 256, GL_RGBA, GL_FLOAT, &data[0]);

        m_vertex_shader = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(m_vertex_shader, 1, &vs_src_texture, NULL);
        glCompileShader(m_vertex_shader);

        m_fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(m_fragment_shader, 1, &fs_src_texture, NULL);
        glCompileShader(m_fragment_shader);

        m_program = glCreateProgram();
        glAttachShader(m_program, m_vertex_shader);
        glAttachShader(m_program, m_fragment_shader);
        glLinkProgram(m_program);
        glUseProgram(m_program);

        glGenVertexArrays(1, &m_vertex_array_object);
        glBindVertexArray(m_vertex_array_object);

        glutMainLoop();
    }

private:
    GLuint m_program;
    GLuint m_vertex_shader;
    GLuint m_fragment_shader;
    GLuint m_vertex_array_object;
    GLuint m_texture;
};

}

int main(int argc, char** argv)
{
    app _app;
    _app.init(argc, argv);
    _app.run();

    return EXIT_SUCCESS;
}
