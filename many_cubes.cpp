
#include <cstdlib>
#include <GL/glew.h>
#include <GL/glut.h>

#include <iostream>
#include <cmath>

#include <vmath.h>

#define STRINGIFY(...) #__VA_ARGS__"\n"

using namespace std;

const char* vs_src = STRINGIFY(
\n
\n#version 430 core
\n
in vec4 position;

out VS_OUT
{
    vec4 color;
} vs_out;

uniform mat4 mv_matrix;
uniform mat4 proj_matrix;

void main()
{
    gl_Position = proj_matrix * mv_matrix * position;
    vs_out.color = position * 2.0 + vec4(0.5, 0.5, 0.5, 0.0);
}

);

const char* fs_src = STRINGIFY(
\n
\n#version 430 core
\n
out vec4 color;

in VS_OUT
{
    vec4 color;
} fs_in;

void main()
{
    color = fs_in.color;
}

);

namespace {

double current_time = 0.0;
float aspect = 0.0;
vmath::mat4 proj_matrix;
GLuint program = 0;
GLint loc_mv_matrix = -1;
GLint loc_proj_matrix = -1;

void idle()
{
    current_time += 0.02;
    glutPostRedisplay();
}

void repaint()
{
    static const GLfloat green[] = { 0.0f, 0.25f, 0.0f, 1.0f };
    static const GLfloat one = 1.0f;
    glClearBufferfv(GL_COLOR, 0, green);
    glClearBufferfv(GL_DEPTH, 0, &one);

    glUseProgram(program);
    glUniformMatrix4fv(loc_proj_matrix, 1, GL_FALSE, proj_matrix);

    for (int i = 0; i < 24; ++i)
    {
        float f = (float)i + (float)current_time * (float)M_PI * 0.3f;

        vmath::mat4 mv_matrix =
            vmath::translate(0.0f, 0.0f, -20.0f) *
            vmath::rotate((float)current_time * 45.0f, 0.0f, 1.0f, 0.0f) *
            vmath::rotate((float)current_time * 21.0f, 1.0f, 0.0f, 0.0f) *
            vmath::translate(
                sinf(2.1f * f) * 2.0f,
                cosf(1.7f * f) * 2.0f,
                sinf(1.3f * f) * cosf(1.5f * f) * 2.0f);

        glUniformMatrix4fv(loc_mv_matrix, 1, GL_FALSE, mv_matrix);

        glDrawArrays(GL_TRIANGLES, 0, 36);
    }

    glutSwapBuffers();
}

void reshape(int width, int height)
{
    cout << "reshape: width=" << width << ", height=" << height << endl;
    aspect = (float)width / (float)height;
    proj_matrix = vmath::perspective(20.0f, aspect, 0.1f, 1000.0f);
}

const GLfloat vertex_positions[] = {
    -0.25f,  0.25f, -0.25f,
    -0.25f, -0.25f, -0.25f,
     0.25f, -0.25f, -0.25f,

     0.25f, -0.25f, -0.25f,
     0.25f,  0.25f, -0.25f,
    -0.25f,  0.25f, -0.25f,

     0.25f, -0.25f, -0.25f,
     0.25f, -0.25f,  0.25f,
     0.25f,  0.25f, -0.25f,

     0.25f, -0.25f,  0.25f,
     0.25f,  0.25f,  0.25f,
     0.25f,  0.25f, -0.25f,

     0.25f, -0.25f,  0.25f,
    -0.25f, -0.25f,  0.25f,
     0.25f,  0.25f,  0.25f,

    -0.25f, -0.25f,  0.25f,
    -0.25f,  0.25f,  0.25f,
     0.25f,  0.25f,  0.25f,

    -0.25f, -0.25f,  0.25f,
    -0.25f, -0.25f, -0.25f,
    -0.25f,  0.25f,  0.25f,

    -0.25f, -0.25f, -0.25f,
    -0.25f,  0.25f, -0.25f,
    -0.25f,  0.25f,  0.25f,

    -0.25f, -0.25f,  0.25f,
     0.25f, -0.25f,  0.25f,
     0.25f, -0.25f, -0.25f,

     0.25f, -0.25f, -0.25f,
    -0.25f, -0.25f, -0.25f,
    -0.25f, -0.25f,  0.25f,

    -0.25f,  0.25f, -0.25f,
     0.25f,  0.25f, -0.25f,
     0.25f,  0.25f,  0.25f,

     0.25f,  0.25f,  0.25f,
    -0.25f,  0.25f,  0.25f,
    -0.25f,  0.25f, -0.25f
};

class app
{
public:
    app() :
        m_program(0),
        m_vertex_array_object(0)
    {
    }

    ~app()
    {
        if (m_vertex_array_object)
            glDeleteVertexArrays(1, &m_vertex_array_object);

        if (m_program)
            glDeleteProgram(m_program);

        program = 0;
    }

    bool init(int argc, char** argv)
    {
        glutInit(&argc, argv);
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
        glutInitWindowSize(1024, 768);
        glutInitWindowPosition(100, 100);
        glutCreateWindow("Many cubes!");

        glutDisplayFunc(repaint);
        glutReshapeFunc(reshape);
        glutIdleFunc(idle);

        glewInit();
        return glewIsSupported("GL_VERSION_3_3");
    }

    void run()
    {
        GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertex_shader, 1, &vs_src, NULL);
        glCompileShader(vertex_shader);

        GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragment_shader, 1, &fs_src, NULL);
        glCompileShader(fragment_shader);

        m_program = glCreateProgram();
        glAttachShader(m_program, vertex_shader);
        glAttachShader(m_program, fragment_shader);
        glLinkProgram(m_program);

        loc_mv_matrix = glGetUniformLocation(m_program, "mv_matrix");
        loc_proj_matrix = glGetUniformLocation(m_program, "proj_matrix");
        cout << "mv_matrix location: " << loc_mv_matrix << endl;
        cout << "proj_matrix location: " << loc_proj_matrix << endl;

        program = m_program;

        glDeleteShader(vertex_shader);
        glDeleteShader(fragment_shader);

        glGenVertexArrays(1, &m_vertex_array_object);
        glBindVertexArray(m_vertex_array_object);

        // Generate some data and put it into a buffer object.
        GLuint buffer;
        glGenBuffers(1, &buffer);
        glBindBuffer(GL_ARRAY_BUFFER, buffer);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertex_positions), vertex_positions, GL_STATIC_DRAW);

        // Set up our vertex attribute.
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        glEnableVertexAttribArray(0);

        glutMainLoop();
    }

private:
    GLuint m_program;
    GLuint m_vertex_array_object;
};

}

int main(int argc, char** argv)
{
    app _app;
    if (!_app.init(argc, argv))
        return EXIT_FAILURE;

    _app.run();

    return EXIT_SUCCESS;
}
